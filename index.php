<?php

use Memory\MemoryGame;

// Autoload to leverage namespacing
require_once __DIR__ . '/vendor/autoload.php';

/**
 * Initialize new 'Memory' game in 6 x 4 grid, with 'Number' theme
 *
 * Themes available:
 *      - Number (default)
 *      - Food
 *      - Doll
 *      - Dragon
 *      - Direction
 *      - Garden
 *      - Season
 */
$memory_game = new MemoryGame(6, 4, 'Number');

// Run game
$memory_game->run();