## Memory Game

[![License](https://poser.pugx.org/leaphly/cart-bundle/license.png)](https://packagist.org/packages/leaphly/cart-bundle)

Test your memory skills with this intuitive Memory Game!

![image](https://dl.dropboxusercontent.com/u/25306307/GitHub/Repos/Memory/Memory.jpg)

### Instructions

Flip 2 tiles, one at a time. If they match, they will disappear from board. If not, they will be flipped back. Try and remember which tile is at which location, so you can match it next time you come across same design.

The objective of the game is to match and remove all tiles, in the least number of moves.

## Getting Started

The following steps will get you set up with a fully functional Memory Game.

### Step 1: Download (Clone repository)

The first step is to download the game, by cloning the repository using `git`.

Use the following command in Terminal: `git clone https://github.com/jaimil/Memory.git`

### Step 2: Install

The app requires downloading of all necessary packages and libraries. Visit `Memory` directory and execute the following command in Terminal: `composer install`

### Step 3: Configure

Open up `index.php` and you will see the following code:

```php
/**
 * Initialize new 'Memory' game in 6 x 4 grid, with 'Number' theme
 *
 * Themes available:
 *      - Number (default)
 *      - Food
 *      - Doll
 *      - Dragon
 *      - Direction
 *      - Garden
 *      - Season
 */
$memory_game = new MemoryGame(6, 4, 'Number');

// Run game
$memory_game->run();
```

When you initialize a new Memory Game, you can specify grid size in `width` by `height` format, as well as `theme` to use. You can choose a number of themes from the following list:

 * Number
 * Food
 * Doll
 * Dragon
 * Direction
 * Garden
 * Season

### Step 4: Run!

The setup is now complete! To run the application, visit `Memory` directory, and execute the following command in Terminal: `php -S localhost:8000`

Visit the following link to access Memory Game:

> [Memory Game (http://localhost:8000)](http://localhost:8000)

## UML Diagram

To access UML diagram for Memory Game, visit the following link:

> [Memory Game UML Diagram](https://dl.dropboxusercontent.com/u/25306307/GitHub/Repos/Memory/MemoryGameUML.jpg)

## Game Structure

Start:

- `index.php` (index.php)

Core:

- `MemoryGame.php` (lib/Memory/MemoryGame.php)
- `Grid.php` (lib/Memory/Grid.php)
- `Card.php` (lib/Memory/Card.php)

Themes:

- `Theme.php` (lib/Memory/Themes/Theme.php)
- `DirectionTheme.php` (lib/Memory/Themes/DirectionTheme.php)
- `DollTheme.php` (lib/Memory/Themes/DollTheme.php)
- `DragonTheme.php` (lib/Memory/Themes/DragonTheme.php)
- `FoodTheme.php` (lib/Memory/Themes/FoodTheme.php)
- `GardenTheme.php` (lib/Memory/Themes/GardenTheme.php)
- `NumberTheme.php` (lib/Memory/Themes/NumberTheme.php)
- `SeasonTheme.php`(lib/Memory/Themes/SeasonTheme.php)

Views:

- `_header.php` (lib/Memory/views/partials/_header.php)
- `_grid.php` (lib/Memory/views/partials/_grid.php)
- `_footer.php` (lib/Memory/views/partials/_footer.php)

Assets:

- `global.css` (assets/css/global.css)
- `global.js` (assets/js/global.js)

Sprite:

- `memory-sprite.jpg` (assets/img/memory-sprite.jpg)

## License

The Memory Game is open-sourced software licensed under the [MIT License](http://opensource.org/licenses/MIT).