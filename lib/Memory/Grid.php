<?php

namespace Memory;

use Memory\Themes\DirectionTheme;
use Memory\Themes\DollTheme;
use Memory\Themes\DragonTheme;
use Memory\Themes\FoodTheme;
use Memory\Themes\GardenTheme;
use Memory\Themes\NumberTheme;
use Memory\Themes\SeasonTheme;

class Grid {

    /**
     * @var Themes\DirectionTheme
     *      Themes\DollTheme
     *      Themes\DragonTheme
     *      Themes\FoodTheme
     *      Themes\GardenTheme
     *      Themes\NumberTheme
     *      Themes\SeasonTheme
     */
    private $theme;

    /**
     * @var int
     */
    private $width;

    /**
     * @var int
     */
    private $height;

    /**
     * @var array
     */
    private $cards = [];

    /**
     * @param $theme
     * @param $width
     * @param $height
     */
    public function __construct($theme, $width, $height)
    {
        $this->theme = $this->setTheme($theme);
        $this->width = $width;
        $this->height = $height;
    }

    /**
     * Set user-provided theme
     *
     * @param $theme
     * @return DirectionTheme|DollTheme|DragonTheme|FoodTheme|GardenTheme|NumberTheme|SeasonTheme
     */
    private function setTheme($theme)
    {
        switch ($theme)
        {
            case 'Food':
                return new FoodTheme;
                break;

            case 'Doll':
                return new DollTheme;
                break;

            case 'Dragon':
                return new DragonTheme;
                break;

            case 'Direction':
                return new DirectionTheme;
                break;

            case 'Garden':
                return new GardenTheme;
                break;

            case 'Season':
                return new SeasonTheme;
                break;

            default:
                return new NumberTheme;
                break;
        }
    }

    /**
     * Set up grid
     */
    public function setUp()
    {
        // Initialize cards
        $this->initializeCards();

        // Display grid
        $this->display();
    }

    /**
     * Initialize cards
     */
    private function initializeCards()
    {
        // Get half of total cards, for distinct values
        $quantity = ($this->width * $this->height) / 2;

        // Initialize random cards
        for ($i = 0; $i < $quantity; $i++)
        {
            $this->cards[] = new Card($this->theme);
        }

        // Duplicate cards to double quantity and match
        $this->cards = array_merge($this->cards, $this->cards);

        // Shuffle order
        shuffle($this->cards);
    }

    /**
     * Display grid
     */
    private function display()
    {
        $counter = 0;

        echo '<div class="cards">';

        for ($i = 0; $i < $this->height; $i++)
        {
            echo '<div class="row">';

            for ($j = 0; $j < $this->width; $j++)
            {
                $this->cards[$counter]->display();

                $counter++;
            }

            echo '</div>';
        }

        echo '
            </div>
            <div class="clear-left"></div>
        ';
    }

}