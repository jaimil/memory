<?php

namespace Memory\Themes;

interface Theme {

    /**
     * Get total images
     *
     * @return mixed
     */
    public function getTotalImages();

    /**
     * Get theme order
     *
     * @return mixed
     */
    public function getOrder();

}