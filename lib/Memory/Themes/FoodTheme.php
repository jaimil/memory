<?php

namespace Memory\Themes;

class FoodTheme implements Theme {

    /**
     * Get total images
     *
     * @return int|mixed
     */
    public function getTotalImages()
    {
        return 9;
    }

    /**
     * Get theme order
     *
     * @return int|mixed
     */
    public function getOrder()
    {
        return 2;
    }

}