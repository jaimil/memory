<?php

namespace Memory\Themes;

class DirectionTheme implements Theme {

    /**
     * Get total images
     *
     * @return int|mixed
     */
    public function getTotalImages()
    {
        return 4;
    }

    /**
     * Get theme order
     *
     * @return int|mixed
     */
    public function getOrder()
    {
        return 5;
    }

}