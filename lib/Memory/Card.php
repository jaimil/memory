<?php

namespace Memory;

use Memory\Themes\Theme;

class Card {

    /**
     * Width of the card
     */
    const WIDTH = 73;

    /**
     * Height of the card
     */
    const HEIGHT = 103;

    /**
     * Left offset of sprite
     */
    const OFFSET_X = 10;

    /**
     * Top offset of sprite
     */
    const OFFSET_Y = 14;

    /**
     * @var Themes\Theme
     */
    private $theme;

    /**
     * @var int
     */
    private $image;

    /**
     * @var string
     */
    private $image_element;

    /**
     * @param Theme $theme
     */
    public function __construct(Theme $theme)
    {
        $this->theme = $theme;

        $this->setUp();
    }

    /**
     * Set up card
     */
    private function setUp()
    {
        // Pick a random image
        $this->pickRandomImage();

        // Create HTML element of picked image
        $this->createImageElement();
    }

    /**
     * Pick a random image
     */
    private function pickRandomImage()
    {
        $this->image = rand(1, $this->theme->getTotalImages());
    }

    /**
     * Create HTML element picked image
     */
    private function createImageElement()
    {
        $this->image_element = '
            <div class="card" data-offset-x="' . $this->calculateOffsetX() . '" data-offset-y="' . $this->calculateOffsetY() . '" data-image="' . $this->image . '">
                <div class="front face"></div>
                <div class="back face" style="display: none;"></div>
            </div>
        ';
    }

    /**
     * Calculate left offset of picked image, from sprite
     *
     * @return int
     */
    private function calculateOffsetX()
    {
        return self::OFFSET_X + (self::WIDTH * ($this->image - 1)) + ($this->image - 1);
    }

    /**
     * Calculate top offset of picked image, from sprite
     *
     * @return int
     */
    private function calculateOffsetY()
    {
        return self::OFFSET_Y + (self::HEIGHT * ($this->theme->getOrder() - 1)) + (self::OFFSET_Y * ($this->theme->getOrder() - 1));
    }

    /**
     * Display card
     */
    public function display()
    {
        echo $this->image_element;
    }

}