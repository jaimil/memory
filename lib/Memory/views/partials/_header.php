<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>Welcome to Memory Game!</title>

    <meta name="author" content="Jaimil Prajapati"
    <meta name="description" content="Test your memory skills with this intuitive Memory Game!">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="assets/css/global.css"/>
</head>
<body>
<div id="container">
    <header>
        <a class="title" href="/"><h1>Memory Game</h1></a>
        <h4>Let's see how sharp your memory is!</h4>
        <a href="https://github.com/jaimil/Memory"><img style="position: absolute; top: 0; right: 0; border: 0;" src="https://camo.githubusercontent.com/652c5b9acfaddf3a9c326fa6bde407b87f7be0f4/68747470733a2f2f73332e616d617a6f6e6177732e636f6d2f6769746875622f726962626f6e732f666f726b6d655f72696768745f6f72616e67655f6666373630302e706e67" alt="Fork me on GitHub" data-canonical-src="https://s3.amazonaws.com/github/ribbons/forkme_right_orange_ff7600.png"></a>
        <hr/>
    </header>
    <div id="content">
        <div id="statistics">
            <strong>Moves:</strong> <span id="moves">0</span>
        </div>