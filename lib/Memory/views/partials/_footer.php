    </div>
    <footer>
        <hr/>
        &copy; <?= date('Y'); ?> Jaimil Prajapati
    </footer>
</div>

<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<script src="assets/js/global.js"></script>
<script>
    // Initialize Memory Game
    MemoryGame.init();
</script>
</body>
</html>