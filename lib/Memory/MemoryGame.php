<?php

namespace Memory;

class MemoryGame {

    /**
     * @var Grid
     */
    private $grid;

    /**
     * @param $width
     * @param $height
     * @param string $theme
     */
    public function __construct($width, $height, $theme = 'Number')
    {
        // Initialize new grid
        $this->grid = new Grid($theme, $width, $height);
    }

    /**
     * Run the 'Memory' game
     */
    public function run()
    {
        // Include 'header' file
        require_once __DIR__ . '/Views/partials/_header.php';

        // Set up grid
        $this->grid->setUp();

        // Include 'footer' file
        require_once __DIR__ . '/Views/partials/_footer.php';
    }

}