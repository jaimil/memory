/**
 * Memory Game object
 * @type {{init: init, initCards: initCards, bindEvents: bindEvents, solve: solve, removeEvents: removeEvents, activateCard: activateCard, checkAndAct: checkAndAct, cleanUp: cleanUp, updateMoves: updateMoves, toggleFlip: toggleFlip, toggleCard: toggleCard, doesItMatch: doesItMatch, didYouWin: didYouWin}}
 */
var MemoryGame = {

    /**
     * Initialize properties
     */
    init: function() {
        $this = this;

        this.cards = $('.card');
        this.card_1 = null;
        this.card_2 = null;

        this.moves = 0;

        // Initialize cards
        this.initCards($.proxy(this.bindEvents, this));
    },

    /**
     * Initialize cards
     *
     * @param callback
     */
    initCards: function(callback) {
        // Loop through each card and appropriately set background position for sprite
        this.cards.each(function() {
            var offsetX = $(this).data('offset-x');
            var offsetY = $(this).data('offset-y');

            $('.front', $(this)).css('background-position', '-10px -833px');
            $('.back', $(this)).css('background-position', '-' + offsetX + 'px ' + '-' + offsetY + 'px');
        });

        // Bind 'click' event
        callback();
    },

    /**
     * Bind 'click' event on cards
     */
    bindEvents: function() {
        // Grab all cards that aren't already flipped
        this.cards = $('.card').not('.flipped');

        // Set 'click' event
        this.cards.on('click', $.proxy(this.solve, this));
    },

    /**
     * Solve selected cards
     *
     * @param e
     */
    solve: function(e) {
        // Temporarily disable 'click' events
        this.removeEvents();

        // Activate/flip selected card
        this.activateCard(e);

        // Check if 2 cards have been flipped; If so, see if they match
        var delay = 300;

        if (this.card_1 && this.card_2) {
            delay = 1000;

            setTimeout(function() {
                $this.checkAndAct();
            }, 1000);
        }

        // Re-bind 'click' event as well as check if won
        setTimeout(function() {
            $this.cleanUp();
        }, delay);
    },

    /**
     * Disable 'click' event on all cards
     */
    removeEvents: function() {
        this.cards.off('click');
    },

    /**
     * Activate card by flipping it, and removing pointer cursor
     *
     * @param e
     */
    activateCard: function(e) {
        var card = $(e.target).parent();

        if (! this.card_1) {
            this.card_1 = card;
        } else {
            this.card_2 = card;
        }

        // Remove cursor pointer, and notify system that its flipped
        this.toggleCard(card, false);

        // Flip card
        this.toggleFlip(card, true);
    },

    /**
     * Update moves, check if card matches and act accordingly
     */
    checkAndAct: function() {
        // Update moves
        this.updateMoves();

        if (this.doesItMatch()) {
            // If matches, fade out and remove both cards
            this.card_1.find('.face').fadeOut(250, function() {
                $(this).remove();
            });

            this.card_2.find('.face').fadeOut(250, function() {
                $(this).remove();
            });
        } else {
            // If does not match, revert both cards to initial state
            this.toggleFlip(this.card_1, false);
            this.toggleCard(this.card_1, true);

            this.toggleFlip(this.card_2, false);
            this.toggleCard(this.card_2, true);
        }

        this.card_1 = null;
        this.card_2 = null;
    },

    /**
     * Re-bind 'click' events and check if won
     */
    cleanUp: function() {
        this.bindEvents();

        if (this.didYouWin()) {
            $('.cards').html('<h1 class="won">Hooray! You win!</h1><a href="/" class="play-again btn btn-lg btn-success">Play again?</a>');
        }
    },

    /**
     * Update moves
     */
    updateMoves: function() {
        this.moves++;

        $('#moves').html(this.moves);
    },

    /**
     * Toggle flipping of card
     *
     * @param card
     * @param flip
     */
    toggleFlip: function(card, flip) {
        var front = $('.front', card);
        var back = $('.back', card);

        if (flip) {
            front.fadeOut(250, function() {
                back.fadeIn(250);
            });
        }

        else {
            back.fadeOut(250, function() {
                front.fadeIn(250);
            });
        }
    },

    /**
     * Toggle card flipped status, as well as cursor pointer
     *
     * @param card
     * @param enable
     */
    toggleCard: function(card, enable) {
        if (enable) {
            card.css('cursor', 'pointer');
            card.removeClass('flipped');
        }

        else {
            card.css('cursor', 'default');
            card.addClass('flipped');
        }
    },

    /**
     * Check if both the cards match
     *
     * @returns {boolean}
     */
    doesItMatch: function() {
        return (this.card_1.data('image') === this.card_2.data('image'));
    },

    /**
     * Check if won
     *
     * @returns {boolean}
     */
    didYouWin: function() {
        return (this.cards.length === 0);
    }

};